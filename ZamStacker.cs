using System;
using System.Collections.Generic;
using System.Linq;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Inventory;
using AOSharp.Common.GameData;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;

namespace ZamStacker
{
    public class Main : AOPluginEntry
    {
        public override void Run(string pluginDir)
        {
            //Temporary and kinda buggy but it works for me so get over it or fix it yourself :P
            Container stackBag = Inventory.Backpacks.FirstOrDefault(x => x.IsOpen);
            if (!stackBag.Equals(null))
            {
                Chat.WriteLine("Stack Bag Contents:");
                foreach (Item item in stackBag.Items)
                {
                    Chat.WriteLine($" {item.Name} = {item.HighId},");
                }
            }else
            {
                Chat.WriteLine("You need to have a bag open.");
            }

            try
            {
                Game.OnUpdate += OnUpdate;

            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private static void StatStacker()
        {
            List<Item> characterItems = Inventory.Items;
            Container stackBag = Inventory.Backpacks.FirstOrDefault(x => x.IsOpen);
            List<int> stackSlots = new List<int>() { (int)EquipSlot.Weap_Hud3, (int)EquipSlot.Cloth_Neck, (int)EquipSlot.Cloth_LeftFinger};

            if (stackBag != null)
            {
                foreach (Item item in characterItems)
                {
                    //Look for an item equiped to either of the slots we want to stack
                    if (stackSlots.Contains(item.Slot.Instance))
                    {
                        //Chat.WriteLine($"{item.Name}::{item.HighId}");
                        Identity stackBagId = stackBag.Identity;
                        Identity bank = new Identity();
                        bank.Type = IdentityType.BankByRef;
                        bank.Instance = stackSlots.ElementAt(stackSlots.IndexOf(item.Slot.Instance));
                        StripItem(bank, stackBag);
                        return;
                    }
                    EquipItem(stackBag);
                }
            }
        }

        private static void StripItem(Identity bank, Container stackBag)
        {
            Network.Send(new ClientContainerAddItem()
            {
                Target = stackBag.Identity,
                Source = bank
            });
        }

        private static void EquipItem(Container stackBag)
        {
            foreach (Item item in stackBag.Items) 
            {
                if (Enum.IsDefined( typeof(StackItems), item.HighId))
                {
                    item.Equip(EquipSlot.Weap_Hud3);
                    item.Equip(EquipSlot.Cloth_Neck);
                    item.Equip(EquipSlot.Cloth_LeftFinger);
                    break;
                }
            }
        }

        private void OnUpdate(object s, float deltaTime)
        {
            StatStacker();
        }
    }
}
public enum StackItems {
    //POH Stuff
    AncientResorativeFungus = 302925,
    AncientAggressiveWebbing = 302924,   
    AncientProtectiveDrone = 302923,
    
    //Subway Huds
    AmalgamatedResearchAttunementDevice = 305986,

    //ACDC
    AlienCombatDirectiveController = 267528,

    //Tier 3 Research Attunement Devices
    ResearchAttunementDeviceOffenseLevelThree = 269414,
    ResearchAttunementDeviceTradeskillLevelThree = 293701,
    ResearchAttunementDeviceHealthLevelThree = 269417,
    ResearchAttunementDeviceNanoTechnologyLevelThree = 269405,
    ResearchAttunementDeviceMedicalLevelThree = 269409,
    ResearchAttunementDeviceDefenseLevelThree = 269411,
    ResearchAttunementDeviceCombatLevelThree = 269403,

    //Tier 2 Research Attunement Devices
    ResearchAttunementDeviceOffenseLevelTwo = 269413,
    ResearchAttunementDeviceHealthLevelTwo = 269416,
    ResearchAttunementDeviceTradeskillLevelTwo = 293699,
    ResearchAttunementDeviceCombatLevelTwo = 269402,
    ResearchAttunementDeviceMedicalLevelTwo = 269408,
    ResearchAttunementDeviceDefenseLevelTwo = 269410,
    ResearchAttunementDeviceNanoTechnologyLevelTwo = 269404,

    //Tier 1 Research Attunement Devices
    ResearchAttunementDeviceDefenseLevelOne = 269412,
    ResearchAttunementDeviceHealthLevelOne = 269418,
    ResearchAttunementDeviceOffenseLevelOne = 269415,
    ResearchAttunementDeviceNanoTechnologyLevelOne = 269406,
    ResearchAttunementDeviceMedicalLevelOne = 269407,
    ResearchAttunementDeviceCombatLevelOne = 269401,
    ResearchAttunementDeviceTradeskillLevelOne = 293693,

    //Clan Token Boards
    ClanAdvancementSunrise = 296369,
    ClanAdvancementDawn = 296368,
    ClanAdvancementLateNight = 296367,
    ClanAdvancementBlossomsofSummer = 296366,
    ClanAdvancementLeavesofSpring = 296365,
    ClanAdvancementTwigofHope = 296364,
    ClanAdvancementDoubleSun = 296370,
    ClanMeritsXanDefenseParagon = 279437,
    ClanMeritsAwakenedCombatParagon = 302912,
    ClanMeritsAwakenedDefenseParagon = 302914,

    //Rings
    QL200IQRing = 84145
}
